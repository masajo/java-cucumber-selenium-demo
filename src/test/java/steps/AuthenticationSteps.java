package steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class AuthenticationSteps {

    private WebDriver driver;
    private ChromeOptions options = new ChromeOptions();

    @Given("^that Jorge wants to log in in order to manage his products$")
    public void that_Jorge_wants_to_log_in_in_order_to_manage_his_products() throws Exception {
        System.setProperty("webdriver.gecko.driver", "./src/test/resources/drivers/chromedriver.exe");
        options.setHeadless(true);
        driver=new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get("http://casagimeno.net/wp-admin");
    }

    @When("^he send the information required to log in$")
    public void he_send_the_information_required_to_log_in() throws Exception {
        WebElement searchbox = driver.findElement(By.name("log"));

        searchbox.clear();

        searchbox.sendKeys("admin");

        searchbox.submit();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Then("^he should be given access to manage his products$")
    public void he_should_be_given_access_to_manage_his_products() throws Exception {

    }
}
