@runThis
Feature: User is able to login into the application

  @login
  Scenario: Log into application with valid credentials
    Given that Jorge wants to log in in order to manage his products
    When he send the information required to log in
    Then he should be given access to manage his products